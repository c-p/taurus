
==============================
Docs for other Taurus versions
==============================

The `default taurus docs <http://taurus-scada.org>`_ reflect the latest tagged
version in the repository.

Docs are also built as artifacts by the CI pipeline for each ref (branch or tag)
pushed to the repository, and they can be browsed as well usingthe following
URL change (REF) by the branch or tag name for which you want to see the docs:

`https://gitlab.com/taurus-org/taurus/-/jobs/artifacts/REF/file/public/index.html?job=doc`

For example, to check the docs for the latest push to `develop`, visit:

https://gitlab.com/taurus-org/taurus/-/jobs/artifacts/develop/file/public/index.html?job=doc

Note that the docs are also automatically built by the pipelines triggered by
Merge Requests. They can be viewed using the "View App" button in the given
Merge Request page.